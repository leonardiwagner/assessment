Assessment Project
========

Hello! I've split the assessment into two projects:

- [/front-end](https://bitbucket.org/leonardiwagner/front-end/src/master/): Web Application front end to manage and toggle customers features.
- [/api](https://bitbucket.org/leonardiwagner/api/src/master/): REST API Backend (which uses PostgreSQL as database) for feature toggle service.

⚠️ The required API for querying features toggle is described on the [bottom  of /api docs](https://bitbucket.org/leonardiwagner/api/src/master/README.md)

- Deployed front-end: [http://52.34.93.238:5000/](http://52.34.93.238:5000/)
- Deployed api: [http://52.34.93.238:8080/api/v1/features](http://52.34.93.238:8080/api/v1/features)
- Live API feature query call: [https://reqbin.com/3pq1gdti](https://reqbin.com/3pq1gdti)

# Running the project
- Via Docker Compose ([Install](https://docs.docker.com/compose/install/)):
    ```bash
    docker-compose up
    ```
    This will start:
    - PostgreSQL on `:5432`
    - `/api` on `:4000`
    - `/front-end` on `:5000`
  

- Manually:
    - Install PostgreSQL ([Link](https://www.postgresql.org/download/))
    - [How to start /front-end](https://bitbucket.org/leonardiwagner/front-end/src/master/README.md)
    - [How to start /api](https://bitbucket.org/leonardiwagner/api/src/master/README.md)

# Considerations
- I didn't have more time to do proper testing:
    - Integration tests covering all cases
    - Integration tests using database instead of mocks
    - Front end tests
    - End to End tests using Selenium on front-end
- I could do more validations and better feedback from wrong inputs

# Continuous Integration and Deployment Pipeline

I used AWS CodeBuild to build, create docker images and to deploy into the server (AWS EC2 Instance).

The build status on souce-control:

![integrated source control](https://assessment-leonardiwagner.s3-us-west-2.amazonaws.com/Screenshot+from+2020-04-26+22-50-07.png)

A build service run:

![building the project](https://assessment-leonardiwagner.s3-us-west-2.amazonaws.com/Screenshot+from+2020-04-24+21-13-57.png)

Create and publish a Docker image to production:

![exporting docker image](https://assessment-leonardiwagner.s3-us-west-2.amazonaws.com/Screenshot+from+2020-04-24+21-14-58.png)

# That's it!

It was fun and I also learned new things while doing this test, I'm very excited to discuss about it in the next step

see you guys there! 